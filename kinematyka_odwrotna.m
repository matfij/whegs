% LEGENDA(KINEMATYKA ODWROTNA):
% 
% r - d?ugo?? nogi: 200mm
%
% d - zadane przemieszczenie: 10m = 10 000mm
% f - k?t o jaki obr�c? si? ko?czyny robota [w stopniach]
% obr - ilo?? ca?kowitych obrot�w ko?czyn


r = 200;

d = 10000;

f = (0.009549365445 * d) * (180 / pi)

obr = f/360