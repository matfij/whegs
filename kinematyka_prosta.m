% LEGENDA(KINEMATYKA PROSTA):
% 
% F -> ?(obr�t centralnego przegubu)
% f -> ?(obrot tylnych n�g)
% p -> ?(obr�t przednich n�g)
% 
% geometria robota: b1, b2, w, h, r
% przemieszczenie: d

syms F 
syms fp
syms fl 
syms yp
syms yl

syms b1
syms b2
syms w
syms h
syms r

syms d

%%%%%%%%%%%%%%%%%%%% POZYCJA ?RODKA PLATFORMY ROBOTA %%%%%%%%%%%%%%%%%%%%%%

D1 = [1 0 0 d; 0 1 0 0; 0 0 1 0; 0 0 0 1] * [1 0 0 0; 0 0 1 0; 0 -1 0 0; 0 0 0 1];

D2 = [1 0 0 0; 0 1 0 0; 0 0 1 r; 0 0 0 1] * [1 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 1];

D = D1*D2



%%%%%%%%%%%%%%%%%%%%%%% POZYCJA TYLNEJ, LEWEJ NOGI %%%%%%%%%%%%%%%%%%%%%%%%

A1 = [1 0 0 -b1; 0 1 0 0; 0 0 1 0; 0 0 0 1];

A2 = [1 0 0 0; 0 1 0 0; 0 0 1 0.5*w; 0 0 0 1];

A3 = [cos(fp) -sin(fp) 0 0; sin(fp) cos(fp) 0 0; 0 0 1 0; 0 0 0 1] * [1 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 1];

A4 = [1 0 0 0; 0 1 0 0; 0 0 1 r; 0 0 0 1] * [1 0 0 0; 0 0 1 0; 0 -1 0 0; 0 0 0 1];

A = A1*A2*A3*A4

%%%%%%%%%%%%%%%%%%%%%%% POZYCJA TYLNEJ, PRAWEJ NOGI %%%%%%%%%%%%%%%%%%%%%%%

Ap1 = [1 0 0 -b1; 0 1 0 0; 0 0 1 0; 0 0 0 1];

Ap2 = [1 0 0 0; 0 1 0 0; 0 0 1 -0.5*w; 0 0 0 1];

Ap3 = [cos(fl) -sin(fl) 0 0; sin(fl) cos(fl) 0 0; 0 0 1 0; 0 0 0 1] * [1 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 1];

Ap4 = [1 0 0 0; 0 1 0 0; 0 0 1 r; 0 0 0 1] * [1 0 0 0; 0 0 1 0; 0 -1 0 0; 0 0 0 1];

Ap = Ap1*Ap2*Ap3*Ap4

%%%%%%%%%%%%%%%%%%%%%% POZYCJA PRZEDNIEJ, LEWEJ NOGI %%%%%%%%%%%%%%%%%%%%%%

B1 = [cos(F) -sin(F) 0 0; sin(F) cos(F) 0 0; 0 0 1 0; 0 0 0 1] * [1 0 0 b2; 0 1 0 0; 0 0 1 0; 0 0 0 1];

B2 = [1 0 0 0; 0 1 0 0; 0 0 1 0.5*w; 0 0 0 1];

B3 = [cos(yp) -sin(yp) 0 0; sin(yp) cos(yp) 0 0; 0 0 1 0; 0 0 0 1] * [1 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 1];

B4 = [1 0 0 0; 0 1 0 0; 0 0 1 r; 0 0 0 1] * [1 0 0 0; 0 0 1 0; 0 -1 0 0; 0 0 0 1];

B = B1*B2*B3*B4

%%%%%%%%%%%%%%%%%%%%% POZYCJA PRZEDNIEJ, PRAWEJ NOGI %%%%%%%%%%%%%%%%%%%%%%

Bp1 = [cos(F) -sin(F) 0 0; sin(F) cos(F) 0 0; 0 0 1 0; 0 0 0 1] * [1 0 0 b2; 0 1 0 0; 0 0 1 0; 0 0 0 1];

Bp2 = [1 0 0 0; 0 1 0 0; 0 0 1 -0.5*w; 0 0 0 1];

Bp3 = [cos(yl) -sin(yl) 0 0; sin(yl) cos(yl) 0 0; 0 0 1 0; 0 0 0 1] * [1 0 0 0; 0 0 -1 0; 0 1 0 0; 0 0 0 1];

Bp4 = [1 0 0 0; 0 1 0 0; 0 0 1 r; 0 0 0 1] * [1 0 0 0; 0 0 1 0; 0 -1 0 0; 0 0 0 1];

Bp = Bp1*Bp2*Bp3*Bp4



